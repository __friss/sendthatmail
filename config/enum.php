<?php
return [
	'flags'=>[
        'danger'=>'Danger', //rouge
        'warning'=>'Avertissement', //jeune
        'info'=>'Information', //bleu
        'message'=>'Message', //gris 
        'success'=>'Succès', //vert ]
    ],
];