<?php
namespace App\Services;

use Storage;
use Session;
use File;
use Helpers;
use App\Models\Attachment;
class AttachmentService
{
	public 	$model;

	public function __construct( Attachment $model)
	{
		$this->model = $model;
	}

	public function fullCleanup(){
		$this->model->all()->each(function($a) {

			if( !($token = Helpers::checkToken($a->token_attachment)) instanceof \Tymon\JWTAuth\Payload){

				$this->deleteFile($a->filepath_attachment);
				$a->delete();
				//dd('one');
			}
		});
		if($this->model->all()->isEmpty()){
			File::cleanDirectory( Storage::disk('attachment')->path('/') ); //remove leftovers
		}

	}

	public function create(array $payload)
    {
        return $this->model->create($payload);
    }

    public function destroy($id)
    {
    	$attachment = $this->find($id);
    	if(!is_null($attachment)){
    		$this->deleteFile($attachment->filename_attachment);
    		return $attachment->delete();
    	}
    	return false;
        //return $this->model->destroy($id);

    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function putFile(\Illuminate\Http\UploadedFile $file, string $name){

		return $file->storeAs('/',$name,['disk'=>'attachment']);
	}

	public function saveFile(\Illuminate\Http\UploadedFile $file){

		if($file->isValid()){
           	$newname = $this->prepareName($file->getClientOriginalName(),Storage::disk('attachment')->path('/'));

           	$this->prepareFilesToCheck( $newname );

            if($file->storeAs('/',$newname,['disk'=>'attachment'])){
            	return $newname;
            }
        }
        return false;
	}

	private function prepareFilesToCheck(string $filename){
		$key = 'task_check_file_upload';
           	$val =array();
           	if(Session::has($key)){
           		$val = Session::get($key);
           	}
           	$val[] = $filename;
           	Session::put($key,$val);
        	Session::save();
	}

	public function deleteFile(string $file){
		return File::delete(Storage::disk('attachment')->path('').DIRECTORY_SEPARATOR.basename($file));
	}

	public function prepareName(string $name, string $dir){
		$ext = substr($name, strrpos($name, '.') + 1);
		$tmp_newname = str_slug(substr($name, 0, strrpos($name, '.')));//.'.'.$ext;
		$i=1;
		do {
			$suffix = ($i>1) ?'-'.$i:null;
			$newname = $tmp_newname.$suffix.'.'.$ext;
			$i++;
		} while ( is_file( $dir.DIRECTORY_SEPARATOR.$newname ) );

		return $newname;
	}

	public function createToken(string $sender, string $filename){
		return Helpers::createToken($sender,$filename);
	}
}