<?php 
namespace App\Traits;

use Illuminate\Http\Request;
use Session;

trait TaskProgressTrait {

	public function initTaskProgress($name=null,$default=0){
        $name = (!is_null($name)) ? $name : uniqid();
        $this->setTaskProgress($name,$default);
    }

    public function setTaskProgress($name,$val=0){

        Session::put('task_progress_'.$name,$val);
        Session::save();
    } 

    public function checkTaskProgress(Request $request,$callback=null){

        $name = 'task_progress_'.$request->name;

        if(Session::has($name))
        {
            $over =boolval($request->over);
            
            if($over){
                $percent = 100;
            }else{
                $percent = Session::get($name);
               // dd(is_callable($callback),$callback);
                
                if(is_callable($callback($percent))){
                	$f=$callback();
                	if($f>0){
                		$percent = $f;
                	}
                }
                $percent++;
            }
            //dd($name,Session::get($name),$request->over,$percent);

            $this->setTaskProgress($request->name,$percent);
            if($over || $percent >= 100){

                   $this->clearTaskProgress($name);
            }

            $out = ['success'=>$percent,'over'=>$over];
        }elseif((int) $request->init == 1){
        	$this->setTaskProgress($request->name,0);
        	$out = ['success'=>0,'over'=>false];
        }
        else{
            $out = ['error'=>'pas d\'info de progression ('.$name.')'];
        }

        if($request->ajax() || $request->wantsJson()){
            return response()->json($out,200);
        }
        return $out;
    }

    public function clearTaskProgress($name){
        if(Session::has($name))
        {

            Session::forget($name);
            Session::forget('task_check_file_upload');
            Session::forget('task_nb_file_upload');
        }
    }
}