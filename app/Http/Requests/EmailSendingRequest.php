<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;


use Illuminate\Validation\Rule;

class EmailSendingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge(['recipients'=>array_values(array_map('trim',array_filter(preg_split('/(\r\n|\n|\r|\t|\,|\;)/i', $this->recipients))))]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $nb_attachment = (is_array($this->attachments)) ? count($this->attachments) : 0;

        $post_max_size = (int) ini_get('post_max_size') * 1024;
        $upload_max_filesize = (int) ini_get('upload_max_filesize') * 1024;

        $reduced_file_post_size = ($post_max_size / $nb_attachment) - 1024;

        $total_max_filesize = $nb_attachment * $upload_max_filesize;

        //dd($upload_max_filesize,$max_post_size,$total_max_filesize);

        if($total_max_filesize < $reduced_file_post_size){
            $rule_max_filesize = $upload_max_filesize;
        }else{
            $rule_max_filesize = $reduced_file_post_size;
        }

       // $apikey_id = $this->route()->apikey;//nom du model =>id
        $rules = [
            'sender'=>'email:rfc,filter',
            'recipients' => 'filled',
            'recipients.*' => 'email:rfc,filter',
            'attachments' => 'nullable',
            'attachments.*' => 'sometimes|file|max:'.$rule_max_filesize,
        ];

        return $rules;
    }

    
   /* public function messages()
    {
        return [
            'profile.nom_profile.required' => __('I love programming.'),
            
        ];
    }*/

    public function response(array $errors)
    {
        if ($this->expectsJson()) {
            return new JsonResponse($errors, 422);
        }

        return $this->redirector->to($this->getRedirectUrl())
                                        ->withInput($this->except($this->dontFlash))
                                        ->withErrors($errors, $this->errorBag)
                                        //->with('alert-danger',__('There are some errors in your sending') )
                                       ;
    }
}
