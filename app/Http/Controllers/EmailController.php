<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EmailSendingRequest;
use App\Jobs\AttachmentStoring;
use App\Jobs\SendEmailJob;
use App\Services\AttachmentService;
use App\Traits\TaskProgressTrait;
use Storage;
use JWTAuth;
use Session;
use File;
use Artisan;

class EmailController extends Controller
{
    use TaskProgressTrait;

    public $attachment_service;

    public function __construct(AttachmentService $attachment_service) {
        $this->attachment_service = $attachment_service;
    }

    public function taskProgress(Request $request){

        return $this->checkTaskProgress($request,function($percent){
            $key = 'task_check_file_upload';
            $i=0;
            $total = Session::get('task_nb_file_upload');
            if(Session::has($key)){
                $files = Session::get($key);

                foreach ($files as $f) {
                    if(File::exists( Storage::disk('attachment')->path('').DIRECTORY_SEPARATOR.basename($f) )){
                        $i++;
                    }
                }

                $res = ($i/$total) * 80;
                if($res>$percent){
                    return $res;
                }

            }
            return $percent;

        });
    }

    public function index()
    {
        //$this->attachment_service->fullCleanup();
        return view('index');
    }




    public function store(EmailSendingRequest $request)
    {
        //php artisan queue:work

        $errors = false;

        $validatedData = $request->except('_token');


        $details=[
         'sender' => $validatedData['sender'],
         'recipients'=>$validatedData['recipients'],
         'attachments'=>(isset($validatedData['attachments'])) ? $validatedData['attachments']:null,
         'base_url' => Storage::disk('attachment')->url('/'),
         'exp'=>null,
        ];

        if(is_array($details['attachments']) && !empty($details['attachments'])){
            Session::put('task_nb_file_upload',count($details['attachments']));
            Session::save();

                // :/
            $me = ini_get('max_execution_time');
            ini_set('max_execution_time',-1);
            foreach ($details['attachments'] as &$file) {


                if(false !== ($newname= $this->attachment_service->saveFile($file))){//
                    if(!empty($token = $this->attachment_service->createToken($validatedData['sender'],$newname))){


                        if(($m = $this->attachment_service->create(['sender_attachment'=>$details['sender'],'filename_attachment'=>$newname,'token_attachment'=>$token,'recipients_attachment'=>implode(',',$details['recipients'])])) instanceof \App\Models\Attachment){

                            $details['exp'] = JWTAuth::decode($token)->getClaims()['exp']->getValue();
                            $details['exp'] = $this->convertDate($details['exp']);

                            $file = ['file'=>$newname,'token'=>$token];
                        }else{
                            $this->attachment_service->deleteFile($newname);
                            $errors[] = __('Cannot save data in database for').' '.$file->getClientOriginalName();
                        }

                    }else{
                        $this->attachment_service->deleteFile($newname);
                        $errors[] = __('Cannot create token for').' '.$file->getClientOriginalName();
                    }
                }else{
                    $errors[] = __('Cannot upload').' '.$file->getClientOriginalName();
                }

            }
            ini_set('max_execution_time',$me);
        }else{
            $errors[] = __('Your sending has to contains at least one valid attachment');
        }

        if(empty($errors)){
            $msg = __('Congrats, your message is on the way!');
            dispatch(new SendEmailJob($details));
            $this->clearTaskProgress('file_upload');

                Artisan::call('queue:work --once');//for dev purpose to avoid typing php artisan queue:work in the terminal
        }else{
            $msg = __('Sorry, some errors occured!');
        }

        if($request->ajax() || $request->wantsJson()) 
        {
            return new \Illuminate\Http\JsonResponse(['success'=>$msg,'errors'=>$errors], 200);
        }else{
            return back()->with(['message'=>$msg,'errors'=>$errors]);
        }
    }

    public function convertDate(int $ts, string $zone = 'Europe/Paris'){
        $date = new \DateTime();
        $date->setTimestamp($ts);
        $date->setTimezone(new \DateTimeZone($zone));
        return $date->format('d/m/Y H:i');
    }
}