<?php
namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Storage;
use JWTAuth;
use Helpers;
use App\Models\Attachment;

class FileController extends Controller
{

	public function show($slug,$token){

		$errors = false;

		$attachment = Attachment::where('filename_attachment',$slug)->firstOrFail();
		if(is_null($token)) abort(404);

		if( ($token = Helpers::checkToken($token)) instanceof \Tymon\JWTAuth\Payload){
			$claims = $token->getClaims();
			if($claims['sub']->getValue()!=$slug){
				$errors['token_corrupted'] = __('Token corrupted');
			}
		}else{
			$errors = $token;
		}


		if(!empty($errors)){
			//dd($errors);
			\Log::info(implode(',',$errors));
			$this->cleanup($attachment);
			abort(404);
		}


		$headers = [
              'Content-Type' => 'application/octet-stream',
           ];

		return response()->download($attachment->filepath_attachment, $slug, $headers);

	}

	private function cleanup(Attachment $attachment){
		app(App\Services\AttachmentService::class)->deleteFile($attachment->filename_attachment);
		$attachment->delete();
	}
}