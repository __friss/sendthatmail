<?php
namespace App\Libraries;

use JWTFactory;
use JWTAuth;

class Helpers {
	
	 /*
		iss: The issuer of the token
		sub: The subject of the token
		aud: The audience of the token
		exp: JWT expiration time defined in Unix time
		nbf: “Not before” time that identifies the time before which the JWT must not be accepted for processing
		iat: “Issued at” time, in Unix time, at which the token was issued
		jti: JWT ID claim provides a unique identifier for the JWT
		*/

    public static function createToken(string $iss, string $sub=null){

    	
    	if(is_null($sub)) $sub = $iss;

    	$customClaims = [ 'iss'=>$iss,'sub' => $sub, ];
		$factory = JWTFactory::customClaims($customClaims);
        $payload = $factory->make();

		$token = JWTAuth::encode($payload);

		if(JWTAuth::decode($token) instanceof \Tymon\JWTAuth\Payload){
			return $token;
		}

		return false;
    }

    public static function checkToken(string $token){
    	try {

			$filetoken = JWTAuth::setToken($token);
			//dd($filetoken);
			$filetoken = JWTAuth::decode($filetoken->getToken()); // trigs exception if bad token

		} catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
			$errors['token_expired'] = __('Token expired');

		} catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
			//dd('2',$e->getCode(),$e->getMessage());
			$errors['token_invalid'] = __('Token invalid');

		} catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
			$errors['token_absent'] = __('Token missing');

		}

		if(!empty($errors)){
			return $errors;
		}

		return $filetoken;
    }
}