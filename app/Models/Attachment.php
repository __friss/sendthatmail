<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


use JWTFactory;
use JWTAuth;
use Storage;
class Attachment extends Model{

	public  $primaryKey = "id_attachment",
			$timestamps = true;


	protected	$table = 'attachments',
				$fillable = [
						'sender_attachment',
						'filename_attachment',
						'recipients_attachment',
						'token_attachment',
						],
				$appends = ['filepath_attachment'];



	public function getFilepathAttachmentAttribute(){ 
        return str_replace(array('/','\\'),DIRECTORY_SEPARATOR,Storage::disk('attachment')->path('').$this->filename_attachment);
    }

}