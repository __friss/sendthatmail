## How to make this work

#### Clone the repo
>git clone git@bitbucket.org:__friss/sendthatmail.git

#### Get the dependencies
>composer update

#### Create your .env from the example

#### Generate the key if not already written in your .env
>php artisan key:generate

#### Update your database crentials in your .env

#### Generate a passphrase for the jwt toekn handling package
>php artisan jwt:secret

#### Set the QUEUE_CONNECTION in your .env
>QUEUE_CONNECTION=database

#### Setup you email sending server credentials and params in your .env
>MAIL_DRIVER=smtp

>MAIL_HOST=smtp.gmail.com

>MAIL_PORT=587

>MAIL_USERNAME=youraddress@gmail.com

>MAIL_PASSWORD=yourpass

>MAIL_ENCRYPTION=tls

>MAIL_FROM_ADDRESS=youraddress@gmail.com

>MAIL_FROM_NAME="${APP_NAME}"

#### Launch the database migration
>php artisan migrate

#### Make the storage symlink
>php artisan storage:link

#### Go to the index page to start testing it via
>php artisan serve
or via the vhost you have previously created

## Notes
Design was intentionnaly omitted.
Dev with Laravel 8, php 7.3.27

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
