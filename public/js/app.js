(function($) {
	'use strict';
    $(function() {

    	$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		console.log('READY');

		var $toBeCloned = $('.attachments [type="file"]').last().parent(),
			check_progress = function(key,over,init){

				over = (typeof over !=='undefined') ? over : false;
				init = (typeof init !=='undefined') ? init : false;
				var urlToCall = '/taskprogress';//'/verifupload';//taskprogress
				$.ajax({
	    			type: 'POST',
	    			//dataType: 'json',
	    			url: urlToCall,
	  				data:{'name':key,'over':Number(over),'init':Number(init)}
	   
				}).fail(function(xhr, status, data) {

				}).done(function(data) {
					console.log(data);
					/*if(data.success==100){
						//clearInterval(inter);
					}*/
					

					if(data.success<100){
						$('.progress .bar').css('width', data.success+'%' );
						$('.progress .percent').text(data.success+'%');
						setTimeout(function(){
							check_progress(key);
						},1000);
					}else{

						if(data.success>=100 || data.over==true){

							$('.progress .bar').css('width', '100%' );
							$('.progress .percent').text('100%');
						}

					}

					/*if(over){
						$('.progress .bar').css('width', '0%' );
						$('.progress .percent').text('');
					}*/

				}).always(function(data){
				});
	};

	//--------------------------------

		$('.add-file').on('click',function(){
			if($('.attachments [type="file"]').last().val()!=''){
				var $c = $toBeCloned.clone();
				$c.find('input').val('');
				$c.find('.remove-file').removeClass('hide');
				$c.appendTo($('.attachments'));
			}else{
				alert('Please fill the empty file field first, thank you');
			}
		});

		$('body').on('click','.remove-file',function(){
			var $p = $(this).parent();
			//console.log($p.index('.attachment'));return;
			if($p.index()==0){
				$p.find('[type="file"]').val('');
			}else{
				$p.remove();
			}
		});


		//---

		$('body').on('submit','.ajx-upload',function(e){

			e.preventDefault();
			$('.messages').empty().addClass('hide');
			$('.progress').removeClass('hide');

			check_progress('file_upload',false,true);

			var $form = $(this),
				formdata = new FormData(),
				dataObj={},
				p,
				err='';

				//simplifié
				$.each($form.find('[type="text"],[type="file"],textarea'),function(k,v){
					if(v.type == 'file' && v.value!='')
					{
						formdata.append(v.name,v.files[0]);
					}else{
						dataObj[v.name] = v.value;
					}
				});

				for(p in dataObj){
					formdata.append(p,dataObj[p]);
				}

				dataObj = formdata;

			$.ajax({
			    type: 'POST',
			    //dataType: 'json',
			    url: $form.attr('action'),
			    processData: false,
				contentType: false,
			    data: (typeof dataObj ==='object' || typeof dataObj ==='string') ? dataObj : {}
			}).fail(function(data, status, error) {

				console.log(data);
				if(typeof data.responseJSON.errors !=='undefined'){

					for(var p in data.responseJSON.errors){
						$('.messages').append( $('<li />',{'html':data.responseJSON.errors[p]}));
						//console.log();
					}
					$('.progress').addClass('hide');
				}
			}).done(function(data,status) {
				//window.location.reload();
				console.log(data);
				if(typeof data.success !=='undefined'){
					$('.progress').addClass('hide');
					$('.messages').append( $('<li />',{'class':'success','html':data.success}));
				}

				//if(typeof data.errors !== 'undefined'){
					//alert(data.errors);
					
					//on fire le cookie de task en php
				//}else{
					check_progress('file_upload',true);
					$('input').attr('disabled','disabled');
					$('textarea').attr('readonly','readonly');
					$('.send-another-mail').removeClass('hide');
				//}
			}).always(function(data,status){
				//console.log(data.responseJSON.message);
				//on dit que c'est finni
				$('.messages').removeClass('hide');
				/*setTimeout(function(){ 
					window.location.reload(); // à voir pour le ctrl f5
				}, 200);*/
			});

			
		});



		//---
	});//ready
})(jQuery);