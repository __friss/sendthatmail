<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

//ok
/*Route::get('email-test', function(){

$details['email'] = 'friss.designs@gmail.com';

dispatch(new App\Jobs\SendEmailJob($details));

dd('done');
});*/


Route::get('/',[
			'as' => 'email.sending',
			'uses' => 'EmailController@index'
		]);

Route::post('/email',[
			'as' => 'email.store',
			'uses' => 'EmailController@store'
		]);

Route::post('/taskprogress',[
	'as' => 'taskprogress',
	'uses' => 'EmailController@taskprogress'
]);
//ok
/*Route::match(array('GET', 'POST'),'/storage/uploads/{slug}', function ($filename) {
    dd($filename);
})->where('slug','[a-zA-Z0-9.\-_\/]+');*/

//Route::match(array('GET', 'POST'), '/storage/uploads/{slug}', 'FileController@show');
Route::get('/files/{slug}/{token}', 'FileController@show');