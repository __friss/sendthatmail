@extends('templates.email_html')


@section('email-content')
{{--  sender
attachments
base_url --}}
<p>
	{{$details['sender']}} {{__('has just sent you')}} 
@if(count($details['attachments'])>1)
 {{__('some files')}}<br>
 {{__('here are the links below to download them')}}
@else
{{__('a file')}}<br>
{{__('here is the link below to download it')}}
@endif
<br>
<sub>
	@if(strlen($details['exp'])>0)
{{__('links are valid until')}} {{$details['exp']}}
@else
	{{__('links are avaiable for one week')}}
@endif
</sub>
</p>
 <table class="action" align="center" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center">
						<table border="0" cellpadding="0" cellspacing="0">

							@foreach($details['attachments'] as $a)
								<tr>
									<td>
										<a href="{{ $details['base_url'].$a['file'].'/'.$a['token'] }}" class="button button-blue" target="_blank">{{ __('Download') }} {{$a['file']}}</a>
										
									</td>
								</tr>

							@endforeach
							
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

@stop