@extends('templates.email_plain')


@section('email-content')
 
 {{$details['sender']}} {{__('has just sent you')}} 
@if(count($details['attachments'])>1)
 {{__('some files')}}
 {{__('here are the links below to download them')}}
@else
{{__('a file')}}
{{__('here is the link below to download it')}}
@endif

	@if(strlen($details['exp'])>0)
{{__('links are valid until')}} {{$details['exp']}}
@else
	{{__('links are avaiable for one week')}}
@endif

 	@foreach($details['attachments'] as $a)
		{{ $details['base_url'].$a['file'].'/'.$a['token'] }}

	@endforeach
 

@stop