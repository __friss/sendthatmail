<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	@include('_partials.headmeta')
	{{--Html::style(url('/').'/css/back/admin.css')--}}
	@yield('styles')
</head>

<body>

	

	@include('_partials._messages')

	
	<section id="content">

		@yield('content')

	</section>

	<footer>
		@include('_partials.footer')
	</footer>

	@section('scripts')
		{{--Html::script('/js/app.js',['defer'])--}}
		<script defer src="{{ asset('js/app.js') }}"></script>

	@endsection

	@include('_partials.scripts')


</body>
</html>