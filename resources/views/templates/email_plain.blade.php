{{ config('app.name') }}

	@yield("email-content")

Cordialement
L'équipe Sendthatmail

© {{ date('Y') }} {{ config('app.name') }}. Tous droits réservés.