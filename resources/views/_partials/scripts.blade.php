@php
	
	$default_vfp = array('appname'=>config('app.name'));

	if(isset($varsFromPhp) && is_array($varsFromPhp))
	{
		$varsFromPhp = array_merge($default_vfp,$varsFromPhp);
	}else{
		$varsFromPhp = $default_vfp;
	}
@endphp

<script>
	var baseUrl = "<?php echo url('/').'/';?>",
		varsFromPhp = @json( $varsFromPhp, JSON_HEX_TAG );
		console.log(varsFromPhp);
</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" ></script>
<script>window.jQuery || document.write('<script src="'+baseUrl+'js/jquery.min.js" defer><\/script>');</script>

@yield('scripts')