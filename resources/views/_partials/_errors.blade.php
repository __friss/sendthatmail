@if ($errors->any())

	<div class="errors-box col-xs-12">
		<h5>
			
				@if(count($errors->all()) > 1)
					@lang('messages.formerrors')
				@else
					@lang('messages.formerror')
				@endif
			
		</h5>
		<ul class="errors-display">

			    @foreach ($errors->all() as $error)
			        <li>{{ $error }}</li>
			    @endforeach

		</ul>
	</div>

@endif