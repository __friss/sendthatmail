<div class="flash-message">
	@php //dump(Session::has('message')); @endphp
	@if(Session::has('message')) 
		<p class="alert alert-message">{!! Session::get('message') !!}<a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>			
	@endif

		@foreach (config('enum.flags') as $k=>$title)
			@if(Session::has('alert-' . $k))
				<p class="alert alert-{{ $k }}">{!!Session::get('alert-' . $k)!!} <a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
</div> 