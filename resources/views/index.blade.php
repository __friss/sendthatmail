@extends('templates.web')
@section('title', config('app.name'))

@section('content')


<style>
	label{
		display:block;
	}
	.hide{
		display:none;
	}

	.progress{
	    position:relative;
	    width:100%;
	    background: #7c7c7c;
	    position:relative;
	    padding:5px;
	}
    .progress .progress-back{
        background:gray;
        z-index: -1;
    }

    .progress .bar{
    	transition-duration: .8s;
        background-color:#63a51c;
        width:0%;
        height:20px;
    }
    .progress .percent{
        position:absolute;
        display:inline-block;
        top:3px;
        left:48%;
        color: white;
        font-weight: bold;
    }

    .attachments
    {
    	margin: 40px 0;
    }

</style>
	<div class="wrapper">

Welcome
<div class="progress hide">
			{{-- 	<div class="bar"></div>
			--}}
			<div class="progress-back"></div>
				<div class="bar"></div >
				<div class="percent"></div>
		</div>
<form action="{{route('email.store')}}" method="post" enctype="multipart/form-data" class="ajx-upload">
	{!! csrf_field() !!}

	<div>
		<label for="sender">{{__('Sender')}}</label>
		<input type="text" name="sender" id="sender" value="fre_fd@hotmail.com">
	</div>

	<div>
		<label for="recipients">{{__('Recipients')}}</label>
		<p>email addresses separated by comma, semicolon or line breaks</p>
		<textarea name="recipients" id="recipients" cols="30" rows="10">friss.designs@gmail.com
			friss.booking@gmail.com
		</textarea>

	</div>

	<div class="attachments">
		<label for="attachments">Fichiers</label>
		<div class="attachment">
			<input name="attachments[]" type="file">
			<a class="remove-file hide" href="javascript:void(0);">X</a>
		</div>
		{{-- <input name="attachments[]" type="file">
		<input name="attachments[]" type="file"> --}}
		<input class="add-file" type="button" value="Add file">
	</div>
	@include('_partials._errors')
	<ul class="messages hide"></ul>
	<div class="send-mail">
		<input type="submit" value="{{__('Send that mail !')}}">
	</div>
	<div class="send-another-mail hide">
		<a href="javascript:(window.location.reload())">Send another e-mail</a>
	</div>

</form>


</div>
@stop